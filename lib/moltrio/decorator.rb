require "moltrio/decorator/version"

module Moltrio
  module Decorator
    def decorator(name)
      prepend Decorator::Module.new(name) { |*args, method|
        prepend Decorator::Module.new(method, &super(*args, method))
        method
      }
      name
    end

    class Module < ::Module
      def initialize(method, &block)
        mod = block.binding.eval('self')

        visibility =
          if mod.protected_method_defined?(method)
            :protected
          elsif mod.private_method_defined?(method)
            :private
          else
            :public
          end

        super() { send(visibility, define_method(method, &block)) }
      end
    end
  end
end
