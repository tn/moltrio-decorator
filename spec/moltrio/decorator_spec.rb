require 'moltrio/decorator'

describe Moltrio::Decorator do
  def make_decorator_module(&block)
    Module.new do
      extend Moltrio::Decorator
      module_eval(&block)
    end
  end

  def make_target_class(decorators, &block)
    Class.new do
      extend decorators
      class_eval(&block)
    end
  end

  context "without arguments" do
    let(:decorators) do
      make_decorator_module do
        decorator def upcase(method)
          -> { super().upcase }
        end
      end
    end

    let(:usage) do
      make_target_class(decorators) do
        upcase def value
          'value'
        end
      end
    end

    context "when the decorated method is called on an instance" do
      it "calls super and apply the modifications" do
        expect(usage.new.value).to eq 'VALUE'
      end
    end
  end

  context "with arguments and block" do
    let(:decorators) do
      make_decorator_module do
        decorator def upcase(method)
          -> (*args, &block) { super(*args, &block).upcase }
        end
      end
    end

    let(:usage) do
      make_target_class(decorators) do
        upcase def value(*args, &block)
          block.call(args[2])
        end
      end
    end

    context "when the decorated method is called on an instance" do
      it "calls super and apply the modifications" do
        expect(usage.new.value(1, :b, :value) { |a| a.to_s }).to eq 'VALUE'
      end
    end
  end

  context "when super() is called inside a block" do
    let(:decorators) do
      make_decorator_module do
        decorator def upcase(method)
          -> (*args, &block) { super(*args, &block).upcase }
        end
      end
    end

    let(:usage) do
      make_target_class(decorators) do
        upcase def value(key)
          {}.fetch(key) { 'value' }
        end
      end
    end

    context "when the decorated method is called on an instance" do
      it "calls super and apply the modifications" do
        expect(usage.new.value(:key)).to eq 'VALUE'
      end
    end
  end

  context "when super() is called in a block outside the original callstack" do
    let(:decorators) do
      make_decorator_module do
        decorator def delayed_upcase(method)
          -> (*args, &block) {
            super_method = self.method(method).super_method
            @delayed = -> { super_method.call(*args, &block).upcase }
            self
          }
        end
      end
    end

    let(:usage) do
      make_target_class(decorators) do
        def run_delayed
          @delayed.call
        end

        delayed_upcase def value
          'value'
        end
      end
    end

    context "when the decorated method is called on an instance" do
      it "calls super and apply the modifications" do
        expect(usage.new.value.run_delayed).to eq 'VALUE'
      end
    end
  end
end
